package cl.unab.plataformaProyecto.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;
import lombok.Data;

/**
 *
 * @author chris
 */
@Entity
@Table(name = "t_usuario")
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UsuarioEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_usuario")
    private Long idUsuario;
    
    @Column(name = "rut_usuario",nullable = false, length = 15)
    private Long rut;

    @Column(name = "username", unique = true, nullable = false)
    private String username;

    @Column(name = "dv_usuario",nullable = true, length = 2)
    private String dv;

    @Column(name = "password")
//    @JsonIgnore
    private String password;
    @Column(name = "nombres",nullable = false)
    private String nombres;
    @Column(name = "apellido_paterno",nullable = false)
    private String apellidoPaterno;
    @Column(name = "apellido_Materno")
    private String apellidoMaterno;
    
    @Column(name = "telefono")
    private String telefono;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "cod_validacion")
    private String codValidacion;


    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "t_usuario_rol", joinColumns
            = @JoinColumn(name = "id_usuario",
                    referencedColumnName = "id_usuario"),
            inverseJoinColumns = @JoinColumn(name = "rol_id",
                    referencedColumnName = "rol_id"))
    private List<RolEntity> roles;
    
    @Column(name = "activo")
    private boolean activo;
    
}
