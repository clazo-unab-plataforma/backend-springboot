package cl.unab.plataformaProyecto.repository;

import cl.unab.plataformaProyecto.entity.UsuarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Christopher Lazo.
 */
public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Long> {

    UsuarioEntity findByUsername(String username);

    @Query("SELECT u FROM UsuarioEntity u WHERE u.username = ?1 and u.activo = 1")
    public UsuarioEntity obtenerUsuarioPorUsername(String username);

    @Query("SELECT u FROM UsuarioEntity u WHERE u.idUsuario = ?1")
    public UsuarioEntity obtenerUsuarioPorId(Long idUsuario);

    @Modifying(clearAutomatically = true)
    @Query("update UsuarioEntity u set u.activo = 0 where u.idUsuario = :idUsuario")
    public void desactivarUsuario(Long idUsuario);

    @Modifying(clearAutomatically = true)
    @Query("update UsuarioEntity u set u.activo = 1 where u.idUsuario = :idUsuario")
    public void activarUsuario(Long idUsuario);
    
    @Modifying(clearAutomatically = true)
    @Query("update UsuarioEntity u set u.codValidacion = :codVerificacion where u.idUsuario = :idUsuario")
    public void actualizarCodigoVerificacion(@Param("codVerificacion") String codVerificacion,@Param("idUsuario") Long idUsuario);
}
