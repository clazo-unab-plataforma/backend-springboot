package cl.unab.plataformaProyecto.repository;

import cl.unab.plataformaProyecto.entity.ParametroEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Christopher Lazo.
 */
public interface ParametroRepository extends JpaRepository<ParametroEntity, Long>{
    
//    @Query("SELECT p FROM Parametro p WHERE p.grupo = ?1")
//    public List<Parametro> obtenerParametrosPorGrupol(String grupo);
//    
//    @Query("SELECT p FROM Parametro p WHERE p.grupo = ?1 and p.codigo = ?2")
//    public ParametroEntity obtenerParametrosPorGrupoyCodigo(String grupo, Integer codigo);
//    
}
