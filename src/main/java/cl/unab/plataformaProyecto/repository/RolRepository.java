package cl.unab.plataformaProyecto.repository;

import cl.unab.plataformaProyecto.entity.RolEntity;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 *
 * @author Christopher Lazo.
 */
public interface RolRepository extends JpaRepository<RolEntity, Long> {
}
