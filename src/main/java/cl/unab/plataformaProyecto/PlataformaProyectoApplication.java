package cl.unab.plataformaProyecto;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlataformaProyectoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlataformaProyectoApplication.class, args);

//        Twilio.init("AC2e714e12331a10a84e49336f84ae8885", "79d0545a57f2235a96b023eddcb1f191");
//        Message message = Message.creator(
//                new com.twilio.type.PhoneNumber("+56977203473"),
//                new com.twilio.type.PhoneNumber("+12058431907"),
//                "asdadasdasd")
//                .create();
//
//        System.out.println(message.getSid());
    }

}
