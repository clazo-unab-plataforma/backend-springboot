package cl.unab.plataformaProyecto.controller;

import cl.unab.plataformaProyecto.dto.MensajeRespuestaDto;
import cl.unab.plataformaProyecto.dto.UsuarioDetalleDto;
import cl.unab.plataformaProyecto.dto.UsuarioInsercionDto;
import cl.unab.plataformaProyecto.exception.CodigoExcepcionEnum;
import cl.unab.plataformaProyecto.exception.PlataformaBusinessException;
import cl.unab.plataformaProyecto.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author Christopher Lazo.
 */
@RestController
@RequestMapping("/api/v1/usuarios")
@Slf4j
@Api(tags = {"Administrador de usuarios"})
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @ApiOperation(value = "Obtiene la informacion de un usuario mediante su username obtenido desde el token de autenticacion",
            response = UsuarioDetalleDto.class)
    @GetMapping
    public ResponseEntity<UsuarioDetalleDto> buscarPorUsername() {
        UsuarioDetalleDto usuario = usuarioService.obtenerUsuarioDesdeToken();
        if (usuario == null) {
            return ResponseEntity.badRequest().build();
        } else {
            return ResponseEntity.ok(usuario);
        }
    }

    @PostMapping(value = "/auth")
    public ResponseEntity<MensajeRespuestaDto> auth(@RequestParam("user") String username, @RequestParam("pwd") String pwd) {

        try {
            return ResponseEntity.ok(usuarioService.primeraAuthUsuario(username, pwd));
        } catch (Exception e) {
            log.error("error", e);
            if (e instanceof PlataformaBusinessException) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
            } else {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, CodigoExcepcionEnum.PLATAFORMA_GLOB_APP_001.getMensaje(), e);
            }
        }
    }

    @ApiOperation(value = "Obtiene la informacion de un usuario mediante su username obtenido desde el token de autenticacion",
            response = UsuarioDetalleDto.class)
    @GetMapping(value = "/{idUsuario}")
    public ResponseEntity<UsuarioDetalleDto> buscarUsuarioPorId(@ApiParam(value = "ID de usuario", required = true) @PathVariable Long idUsuario) {
        UsuarioDetalleDto usuario = usuarioService.obtenerUsuarioPorID(idUsuario);
        if (usuario == null) {
            return ResponseEntity.badRequest().build();
        } else {
            return ResponseEntity.ok(usuario);
        }
    }

    @ApiOperation(value = "Inserta un usuario",
            response = UsuarioDetalleDto.class)
    @PostMapping
    public ResponseEntity<UsuarioDetalleDto> insertarUsuario(@RequestBody UsuarioInsercionDto usuario) {
        try {
            UsuarioDetalleDto usuarioInsertado = usuarioService.insertarUsuario(usuario);
            if (usuarioInsertado == null) {
                return ResponseEntity.badRequest().build();
            } else {
                return ResponseEntity.ok(usuarioInsertado);
            }
        } catch (Exception e) {
            log.error("error", e);
            if (e instanceof PlataformaBusinessException) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
            } else {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, CodigoExcepcionEnum.PLATAFORMA_GLOB_APP_001.getMensaje(), e);
            }
        }
    }

    @ApiOperation(value = "Modifica un usuario",
            response = UsuarioDetalleDto.class)
    @PutMapping
    public ResponseEntity<UsuarioDetalleDto> modificarUsuario(@RequestBody UsuarioInsercionDto usuario) {
        try {
            UsuarioDetalleDto usuarioInsertado = usuarioService.editarUsuario(usuario);
            if (usuarioInsertado == null) {
                return ResponseEntity.badRequest().build();
            } else {
                return ResponseEntity.ok(usuarioInsertado);
            }
        } catch (Exception e) {
            log.error("error", e);
            if (e instanceof PlataformaBusinessException) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
            } else {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, CodigoExcepcionEnum.PLATAFORMA_GLOB_APP_001.getMensaje(), e);
            }
        }
    }

    @ApiOperation(value = "desactiva un usuario",
            response = MensajeRespuestaDto.class)
    @PutMapping(value = "/desactivar/{idUsuario}")
    public ResponseEntity<MensajeRespuestaDto> desactivarUsuario(@ApiParam(value = "ID de usuario", required = true) @PathVariable Long idUsuario) {
        try {
            usuarioService.desactivarUsuario(idUsuario);
            return ResponseEntity.ok(new MensajeRespuestaDto("Usuario desactivado"));
        } catch (Exception e) {
            log.error("error", e);
            if (e instanceof PlataformaBusinessException) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
            } else {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, CodigoExcepcionEnum.PLATAFORMA_GLOB_APP_001.getMensaje(), e);
            }
        }
    }

    @ApiOperation(value = "activar un usuario",
            response = MensajeRespuestaDto.class)
    @PutMapping(value = "/activar/{idUsuario}")
    public ResponseEntity<MensajeRespuestaDto> activarUsuario(@ApiParam(value = "ID de usuario", required = true) @PathVariable Long idUsuario) {
        try {
            usuarioService.activarUsuario(idUsuario);
            return ResponseEntity.ok(new MensajeRespuestaDto("Usuario activado"));
        } catch (Exception e) {
            log.error("error", e);
            if (e instanceof PlataformaBusinessException) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
            } else {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, CodigoExcepcionEnum.PLATAFORMA_GLOB_APP_001.getMensaje(), e);
            }
        }
    }
}
