package cl.unab.plataformaProyecto.serviceImpl;

import cl.unab.plataformaProyecto.dto.GrupoParametroDto;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cl.unab.plataformaProyecto.dto.GrupoParametrosDto;
import cl.unab.plataformaProyecto.dto.ParametroDto;
import cl.unab.plataformaProyecto.entity.ParametroEntity;
import cl.unab.plataformaProyecto.exception.CodigoExcepcionEnum;
import cl.unab.plataformaProyecto.exception.PlataformaBusinessException;
import cl.unab.plataformaProyecto.repository.ParametroRepository;
import cl.unab.plataformaProyecto.service.ParametroService;
import java.text.ParseException;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Christopher Lazo.
 */
@Service
public class ParametroServiceImpl implements ParametroService {
    
    @Autowired
    ParametroRepository parametroRepository;
    
    @PersistenceContext
    EntityManager em;
    
    private final Integer COD_FECHA_ACTUALIZACION = 2;
    
    @Override
    public List<ParametroDto> obtenerParametrosPorGrupo(String grupo) {
        
        GrupoParametrosDto grupoParametro = GrupoParametrosDto.getInstance();
        List<ParametroDto> listaParametrosFiltrada = grupoParametro.getListaParametros().stream().
                filter(param -> grupo.equals(param.getGrupo())).collect(Collectors.toList());
        
        return listaParametrosFiltrada;
        
    }
    
    @Override
    public List<ParametroDto> obtenerParametros() {
        
        GrupoParametrosDto grupoParametro = GrupoParametrosDto.getInstance();
        return grupoParametro.getListaParametros();
    }
    
    @Override
    public ParametroDto parametroEntityADto(ParametroEntity parametroEntity) {
        if (parametroEntity == null) {
            return null;
        }
        ParametroDto parametroDto = new ParametroDto();
        parametroDto.setIdParametro(parametroEntity.getId());
        parametroDto.setGrupo(parametroEntity.getGrupo());
        parametroDto.setCodigo(parametroEntity.getCodigo());
        parametroDto.setDescripcion(parametroEntity.getDescripcion());
        
        return parametroDto;
    }
    
    public ParametroEntity parametroDtoAEntity(ParametroDto parametroDto) {
        if (parametroDto == null) {
            return null;
        }
        ParametroEntity parametroEntity = new ParametroEntity();
        
        parametroEntity.setId(parametroDto.getIdParametro());
        parametroEntity.setGrupo(parametroDto.getGrupo());
        parametroEntity.setCodigo(parametroDto.getCodigo());
        parametroEntity.setDescripcion(parametroDto.getDescripcion());
        
        return parametroEntity;
    }
    
    @Override
    public ParametroDto obtenerParametrosPorGrupoYCodigo(String grupo, Integer codigo) {
        
        GrupoParametrosDto grupoParametro = GrupoParametrosDto.getInstance();
        Optional<ParametroDto> parametro = grupoParametro.getListaParametros().stream().
                filter(param -> grupo.equals(param.getGrupo())
                && codigo.equals(param.getCodigo())).findFirst();
        
        if (parametro.isPresent()) {
            return parametro.get();
        } else {
            return null;
        }
    }
    
    @Override
    public void cargarParametros() { 
        
        List<ParametroEntity> listaParametroEntity = parametroRepository.findAll();
        List<ParametroDto> listaParametroDto = new ArrayList<ParametroDto>();
        
        listaParametroEntity.forEach(parametro -> {
            listaParametroDto.add(parametroEntityADto(parametro));
        });
        GrupoParametrosDto grupoParametro = GrupoParametrosDto.getInstance();
        grupoParametro.setListaParametros(listaParametroDto);
    }
    
    @Override
    public void actualizarParametros() {
        cargarParametros();
    }

    @Override
    public List<ParametroDto> obtenerParametrosDesdeBD() {
        List<ParametroDto> listaParametroDto = new ArrayList<ParametroDto>();
        parametroRepository.findAll().forEach((parametro) -> {
            listaParametroDto.add(parametroEntityADto(parametro));
        });
        
        return listaParametroDto;
    }
}
