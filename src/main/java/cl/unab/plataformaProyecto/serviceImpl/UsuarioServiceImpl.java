package cl.unab.plataformaProyecto.serviceImpl;

import cl.unab.plataformaProyecto.dto.MensajeRespuestaDto;
import cl.unab.plataformaProyecto.dto.RolDto;
import cl.unab.plataformaProyecto.dto.SmsRequest;
import cl.unab.plataformaProyecto.dto.UsuarioDetalleDto;
import cl.unab.plataformaProyecto.dto.UsuarioDto;
import cl.unab.plataformaProyecto.dto.UsuarioInsercionDto;
import cl.unab.plataformaProyecto.dto.UsuarioInternoDto;
import cl.unab.plataformaProyecto.entity.RolEntity;
import cl.unab.plataformaProyecto.entity.UsuarioEntity;
import cl.unab.plataformaProyecto.exception.CodigoExcepcionEnum;
import cl.unab.plataformaProyecto.exception.PlataformaApplicationException;
import cl.unab.plataformaProyecto.exception.PlataformaBusinessException;
import cl.unab.plataformaProyecto.repository.UsuarioRepository;
import cl.unab.plataformaProyecto.service.ParametroService;
import cl.unab.plataformaProyecto.service.RolService;
import cl.unab.plataformaProyecto.service.SmsService;
import cl.unab.plataformaProyecto.service.UsuarioService;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author chris
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {

    private static final Logger log = LoggerFactory.getLogger(UsuarioServiceImpl.class);

    @PersistenceContext
    EntityManager em;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private RolService rolService;

    @Autowired
    private ParametroService parametroService;

    @Autowired
    @Qualifier("twilio")
    private SmsService smsService;

    private final boolean ESTADO_ACTIVO = true;
    private final Long ROL_ADMIN = 3L;

    private final Integer RANDOM_MIN = 10000;
    private final Integer RANDOM_MAX = 99999;

    @Value("${plataforma.usuario.error.noEncontrado}")
    private String usuarioNoEncontrado;

    @Value("${plataforma.usuario.exito.primeraAutenticacion}")
    private String usuarioExitoAuth;

    @Value("${twilio.message}")
    private String messageTemplate;

    public UsuarioDetalleDto buscarUsuarioPorUsername(String username) {
        return UsuarioDetalleEntityADto(usuarioRepository.obtenerUsuarioPorUsername(username));
    }

    private UsuarioDetalleDto UsuarioDetalleEntityADto(UsuarioEntity usuarioEntity) {

        if (usuarioEntity == null) {
            return null;
        }

        UsuarioDetalleDto usuarioDto = new UsuarioDetalleDto();
        usuarioDto.setIdUsuario(usuarioEntity.getIdUsuario());
        usuarioDto.setRut(usuarioEntity.getRut());
        usuarioDto.setDv(usuarioEntity.getDv());
        usuarioDto.setUsername(usuarioEntity.getUsername());
        usuarioDto.setNombres(usuarioEntity.getNombres());
        usuarioDto.setApellidoPaterno(usuarioEntity.getApellidoPaterno());
        usuarioDto.setApellidoMaterno(usuarioEntity.getApellidoMaterno());
        usuarioDto.setTelefono(usuarioEntity.getTelefono());
        usuarioDto.setEmail(usuarioEntity.getEmail());
        usuarioDto.setActivo(usuarioEntity.isActivo());
        usuarioDto.setTienePassword((usuarioEntity.getPassword() != null && !usuarioEntity.getPassword().isEmpty()));

        List<RolDto> listaRolDto = new ArrayList<RolDto>();
        List<RolEntity> listaRolEntity = usuarioEntity.getRoles();
        for (RolEntity rol : listaRolEntity) {
            listaRolDto.add(rolService.RolEntityADto(rol));
        }
        usuarioDto.setRoles(listaRolDto);

        return usuarioDto;
    }

    @Override
    public UsuarioDetalleDto obtenerUsuarioDesdeToken() {
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        String username = null;
        if (authentication.getPrincipal() instanceof UserDetails) {
            username = ((UserDetails) authentication.getPrincipal()).getUsername();
        } else if (authentication.getPrincipal() instanceof String) {
            username = (String) authentication.getPrincipal();
        } else {
            throw new PlataformaBusinessException(CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_006);
        }

        return buscarUsuarioPorUsername(username);
    }

    @Override
    public boolean usuarioEsAdministrador(UsuarioDetalleDto usuario) {
        boolean respuesta = false;
        for (RolDto rol : usuario.getRoles()) {
            if (rol.getId() == ROL_ADMIN) {
                respuesta = true;
                break;
            }
        }
        return respuesta;
    }

    private List<RolDto> obtenerRolesPorUsuario(Long idUsuario) {
        StringBuilder query = new StringBuilder();
        query.append("select NEW cl.unab.plataformaProyecto.dto.RolDto(");
        query.append(" r.id, ");
        query.append(" r.nombreRol) ");
        query.append(" from UsuarioEntity u join u.roles r ");
        query.append(" where u.idUsuario = :idUsuario");
        query.append(" order by r.id desc");

        TypedQuery<RolDto> queryType = em.createQuery(query.toString(), RolDto.class);

        queryType.setParameter("idUsuario", idUsuario);

        return queryType.getResultList();

    }

    @Override
    public UsuarioDetalleDto insertarUsuario(UsuarioInsercionDto usuario) {
        UsuarioEntity usuarioPersistir = usuarioDtoAEntity(usuario);
        if (usuarioPersistir == null) {
            throw new PlataformaBusinessException(CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_001);
        }
        UsuarioDetalleDto usuarioExistente = buscarUsuarioPorUsername(usuario.getUsername());
        if (usuarioExistente != null) {
            throw new PlataformaBusinessException(CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_006);
        }

        try {
            // cifrado de password
            if (usuario.getPassword() != null && !usuario.getPassword().isEmpty()) {
                // cifrado de password
                BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
                usuarioPersistir.setPassword(bc.encode(usuario.getPassword()));
            }
            usuarioPersistir.setActivo(ESTADO_ACTIVO);
            return UsuarioDetalleEntityADto(usuarioRepository.save(usuarioPersistir));
        } catch (Exception e) {
            log.error("error al insertar usuario: ", e);
            throw new PlataformaApplicationException(e, CodigoExcepcionEnum.PLATAFORMA_USU_APP_001);
        }
    }

    @Override
    public UsuarioEntity usuarioDtoAEntity(UsuarioDto usuario) {

        if (usuario != null) {
            UsuarioEntity usuarioPersistir = new UsuarioEntity();
            usuarioPersistir.setRut(usuario.getRut());
            usuarioPersistir.setDv(usuario.getDv());
            usuarioPersistir.setUsername(usuario.getUsername());
            usuarioPersistir.setNombres(usuario.getNombres());
            usuarioPersistir.setApellidoPaterno(usuario.getApellidoPaterno());
            usuarioPersistir.setApellidoMaterno(usuario.getApellidoMaterno());
            usuarioPersistir.setTelefono(usuario.getTelefono());
            usuarioPersistir.setEmail(usuario.getEmail());
            usuarioPersistir.setActivo(usuario.isActivo());

            List<RolEntity> roles = new ArrayList<RolEntity>();

            if (usuario.getRoles() != null) {
                usuario.getRoles().forEach((rol)
                        -> roles.add(em.getReference(RolEntity.class, rol.getId()))
                );
            }

            usuarioPersistir.setRoles(roles);
            return usuarioPersistir;
        } else {
            throw new PlataformaBusinessException(CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_002);
        }
    }

    @Override
    public UsuarioDetalleDto editarUsuario(UsuarioInsercionDto usuario) {
        if (usuario == null) {
            throw new PlataformaBusinessException(CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_002);
        }

        UsuarioEntity usuarioAntiguo = usuarioRepository.findByUsername(usuario.getUsername());
        if (usuarioAntiguo == null) {
            throw new PlataformaBusinessException(CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_003);
        }

        UsuarioEntity usuarioAModificar = usuarioDtoAEntity(usuario);

        //Se mantienen valor que no son modificables
        usuarioAModificar.setIdUsuario(usuarioAntiguo.getIdUsuario());
        usuarioAModificar.setActivo(usuarioAntiguo.isActivo());

        try {
            if (usuario.getPassword() != null && !usuario.getPassword().trim().isEmpty()) {
                //Cifrado de clave
                BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
                usuarioAModificar.setPassword(bc.encode(usuario.getPassword()));
            } else {
                usuarioAModificar.setPassword(usuarioAntiguo.getPassword());
            }
            return UsuarioDetalleEntityADto(usuarioRepository.save(usuarioAModificar));
        } catch (Exception e) {
            log.error("error al editar usuario: ", e);
            throw new PlataformaApplicationException(e, CodigoExcepcionEnum.PLATAFORMA_USU_APP_002);
        }
    }

    @Override
    @Transactional
    public void desactivarUsuario(Long idUsuario) {

        if (!usuarioRepository.findById(idUsuario).isPresent()) {
            throw new PlataformaBusinessException(CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_004);
        }

        try {
            usuarioRepository.desactivarUsuario(idUsuario);
        } catch (Exception e) {
            log.error("error al desactivar usuario: ", e);
            throw new PlataformaApplicationException(e, CodigoExcepcionEnum.PLATAFORMA_USU_APP_003);
        }
    }

    @Override
    @Transactional
    public void activarUsuario(Long idUsuario) {
        if (!usuarioRepository.findById(idUsuario).isPresent()) {
            throw new PlataformaBusinessException(CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_005);
        }

        try {
            usuarioRepository.activarUsuario(idUsuario);
        } catch (Exception e) {
            log.error("error al activar usuario: ", e);
            throw new PlataformaApplicationException(e, CodigoExcepcionEnum.PLATAFORMA_USU_APP_004);
        }
    }

    @Override
    public UsuarioDetalleDto obtenerUsuarioPorID(Long idUsuario) {
        return UsuarioDetalleEntityADto(usuarioRepository.obtenerUsuarioPorId(idUsuario));
    }

    public UsuarioDetalleDto usuarioDtoAUsuarioDetalleDto(UsuarioDto usuario) {

        if (usuario != null) {
            UsuarioDetalleDto usuarioDetalle = new UsuarioDetalleDto();
            usuarioDetalle.setRut(usuario.getRut());
            usuarioDetalle.setDv(usuario.getDv());
            usuarioDetalle.setUsername(usuario.getUsername());
            usuarioDetalle.setNombres(usuario.getNombres());
            usuarioDetalle.setApellidoPaterno(usuario.getApellidoPaterno());
            usuarioDetalle.setApellidoMaterno(usuario.getApellidoMaterno());
            usuarioDetalle.setActivo(usuario.isActivo());
            usuarioDetalle.setTelefono(usuario.getApellidoMaterno());
            usuarioDetalle.setEmail(usuario.getApellidoMaterno());
            usuarioDetalle.setRoles(usuario.getRoles());

            return usuarioDetalle;
        } else {
            throw new PlataformaBusinessException(CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_002);
        }
    }

    @Override
    public UsuarioInternoDto obtenerUsuarioInternoPorUsername(String username) {
        StringBuilder query = new StringBuilder();
        query.append("select NEW cl.unab.plataformaProyecto.dto.UsuarioInternoDto(");
        query.append(" u.idUsuario,");
        query.append(" u.rut,");
        query.append(" u.username,");
        query.append(" u.password,");
        query.append(" u.telefono,");
        query.append(" u.codValidacion)");
        query.append(" from UsuarioEntity u");
        query.append(" where u.username = :username");
        query.append(" and u.activo = 1");
        TypedQuery<UsuarioInternoDto> queryType = em.createQuery(query.toString(), UsuarioInternoDto.class);

        queryType.setParameter("username", username);

        try {

            UsuarioInternoDto usuarioInterno = queryType.getSingleResult();
            if (usuarioInterno == null) {
                throw new PlataformaBusinessException(CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_006);
            }
            usuarioInterno.setRoles(obtenerRolesPorUsuario(usuarioInterno.getIdUsuario()));

            return usuarioInterno;
        } catch (Exception e) {
            log.error("error al obtener usuario: ", e);
            throw new PlataformaBusinessException(CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_006);
        }

    }

    @Transactional
    @Override
    public MensajeRespuestaDto primeraAuthUsuario(String username, String password) {
        UsuarioInternoDto user = obtenerUsuarioInternoPorUsername(username);
        if (user == null) {
            throw new PlataformaBusinessException(usuarioNoEncontrado, CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_008);
        }

        BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
        if (!bc.matches(password, user.getPassword())) {
            throw new PlataformaBusinessException(usuarioNoEncontrado, CodigoExcepcionEnum.PLATAFORMA_USU_BIZ_008);
        }

        System.out.println("usuario: " + user.toString());

        // generacion de codigo de seguridad
        Random rand = new Random();
        String codigoVerificacion = String.valueOf(rand.nextInt(RANDOM_MAX - RANDOM_MIN) + RANDOM_MIN);

        System.out.println("codigo verificacion: " + codigoVerificacion);
        // Se actualiza usuario en BD
        usuarioRepository.actualizarCodigoVerificacion(codigoVerificacion, user.getIdUsuario());

        // Envio de SMS con codigo de validacion
        SmsRequest msjeSMS = new SmsRequest(user.getTelefono(), String.format(messageTemplate, codigoVerificacion));
        smsService.sendSms(msjeSMS);

        return new MensajeRespuestaDto(usuarioExitoAuth);
    }

}
