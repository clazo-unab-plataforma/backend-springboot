package cl.unab.plataformaProyecto.serviceImpl;

import cl.unab.plataformaProyecto.dto.UsuarioInternoDto;
import cl.unab.plataformaProyecto.service.UsuarioService;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Christopher Lazo.
 */
@Component
public class CustomAuthenticationProviderService implements AuthenticationProvider {

    @Value("${plataforma.usuario.error.noEncontrado}")
    private String usuarioNoEncontrado;

    private static final Logger log = LoggerFactory.getLogger(CustomAuthenticationProviderService.class);

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            String username = authentication.getName();
            String codVerificacion = (String) authentication.getCredentials();

            //Complementar informacion de usuario
            UsuarioInternoDto user = usuarioService.obtenerUsuarioInternoPorUsername(username);

            if (user == null) {
                throw new BadCredentialsException(usuarioNoEncontrado);
            }

            if (!user.getCodValidacion().equals(codVerificacion)) {
                throw new BadCredentialsException(usuarioNoEncontrado);
            }

            List<GrantedAuthority> authorities = new ArrayList<>();
            user.getRoles().forEach(role -> {
                authorities.add(new SimpleGrantedAuthority(role.getNombreRol()));
            });
            return new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), authorities);
        } catch (Exception e) {
            if (e instanceof BadCredentialsException == false) {
                log.error("error en la autenticacion: ", e);
            }
            return null;
        }

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
