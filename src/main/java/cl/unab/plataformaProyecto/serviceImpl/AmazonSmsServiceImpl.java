package cl.unab.plataformaProyecto.serviceImpl;

import cl.unab.plataformaProyecto.dto.SmsRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author Christopher Lazo.
 */
@Service("amazon")
public class AmazonSmsServiceImpl extends SmsServiceImpl {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AmazonSmsServiceImpl.class);
    
    @Override
    public void sendSms(SmsRequest smsRequest) {
        LOGGER.debug("implementar llamada a servicio de Amazon");
    }
    
}
