package cl.unab.plataformaProyecto.serviceImpl;

import cl.unab.plataformaProyecto.dto.SmsRequest;
import cl.unab.plataformaProyecto.service.SmsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Christopher Lazo.
 */
public abstract class SmsServiceImpl implements SmsService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SmsServiceImpl.class);

    @Override
    public void sendSms(SmsRequest smsRequest) {
        LOGGER.debug("implementacion se realiza en clases hijas");
    }

    @Override
    public boolean isValidNumber(String number) {
        return (number != null && !number.isEmpty());
    }

}
