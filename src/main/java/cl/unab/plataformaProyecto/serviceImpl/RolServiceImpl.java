package cl.unab.plataformaProyecto.serviceImpl;

import cl.unab.plataformaProyecto.dto.RolDto;
import cl.unab.plataformaProyecto.entity.RolEntity;
import cl.unab.plataformaProyecto.service.RolService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author Christopher Lazo.
 */
@Service
public class RolServiceImpl implements RolService {

    @PersistenceContext
    EntityManager em;

    @Override
    public RolDto RolEntityADto(RolEntity rolEntity) {

        if (rolEntity == null) {
            return null;
        }

        RolDto rolDto = new RolDto();
        rolDto.setId(rolEntity.getId());
        rolDto.setNombreRol(rolEntity.getNombreRol());

        return rolDto;

    }

    @Override
    public RolEntity RolDtoAEntity(RolDto rolDto) {

        if (rolDto == null) {
            return null;
        }

        RolEntity rolEntity = new RolEntity();
        rolEntity.setId(rolDto.getId());
        rolEntity.setNombreRol(rolDto.getNombreRol());

        return rolEntity;
    }

}
