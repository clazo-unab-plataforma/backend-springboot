package cl.unab.plataformaProyecto.exception;

/**
 *
 * @author Christopher Lazo.
 */
public class PlataformaBusinessException extends PlataformaException {

    public PlataformaBusinessException(CodigoExcepcionEnum codigo) {
        super(codigo);
    }

    public PlataformaBusinessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, CodigoExcepcionEnum codigo) {
        super(message, cause, enableSuppression, writableStackTrace, codigo);
    }

    public PlataformaBusinessException(String message, Throwable cause, CodigoExcepcionEnum codigo) {
        super(message, cause, codigo);
    }

    public PlataformaBusinessException(String message, CodigoExcepcionEnum codigo) {
        super(message, codigo);
    }

    public PlataformaBusinessException(Throwable cause, CodigoExcepcionEnum codigo) {
        super(cause, codigo);
    }

}
