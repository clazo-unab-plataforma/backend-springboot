package cl.unab.plataformaProyecto.exception;

/**
 *
 * @author Christopher Lazo.
 */
public abstract class PlataformaException extends RuntimeException {

    private static final long serialVersionUID = -5790776812436196994L;

    private final CodigoExcepcionEnum codigo;

    public PlataformaException(CodigoExcepcionEnum codigo) {
        this.codigo = codigo;
    }

    public PlataformaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, CodigoExcepcionEnum codigo) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.codigo = codigo;
    }

    public PlataformaException(String message, Throwable cause, CodigoExcepcionEnum codigo) {
        super(message, cause);
        this.codigo = codigo;
    }

    public PlataformaException(String message, CodigoExcepcionEnum codigo) {
        super(message);
        this.codigo = codigo;
    }

    public PlataformaException(Throwable cause, CodigoExcepcionEnum codigo) {
        super(cause);
        this.codigo = codigo;
    }

    public CodigoExcepcionEnum getCodigo() {
        return codigo;
    }

}
