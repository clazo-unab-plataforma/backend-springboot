package cl.unab.plataformaProyecto.exception;

/**
 *
 * @author Christopher Lazo.
 */
public class PlataformaApplicationException extends PlataformaException {

    public PlataformaApplicationException(CodigoExcepcionEnum codigo) {
        super(codigo);
    }

    public PlataformaApplicationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, CodigoExcepcionEnum codigo) {
        super(message, cause, enableSuppression, writableStackTrace, codigo);
    }

    public PlataformaApplicationException(String message, Throwable cause, CodigoExcepcionEnum codigo) {
        super(message, cause, codigo);
    }

    public PlataformaApplicationException(String message, CodigoExcepcionEnum codigo) {
        super(message, codigo);
    }

    public PlataformaApplicationException(Throwable cause, CodigoExcepcionEnum codigo) {
        super(cause, codigo);
    }

}
