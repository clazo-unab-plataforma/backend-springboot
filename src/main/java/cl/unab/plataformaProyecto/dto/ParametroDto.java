package cl.unab.plataformaProyecto.dto;

import lombok.Data;

/**
 *
 * @author Christopher Lazo.
 */
@Data
public class ParametroDto {
    
    private Long idParametro;
    private String grupo;
    private Integer codigo;
    private String descripcion;
    
}
