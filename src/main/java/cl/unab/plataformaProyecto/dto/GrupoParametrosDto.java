package cl.unab.plataformaProyecto.dto;

import java.util.List;
import lombok.Data;

/**
 *
 * @author Christopher Lazo.
 */
@Data
public class GrupoParametrosDto {

    private static GrupoParametrosDto instance;
    private List<ParametroDto> listaParametros;

    private GrupoParametrosDto() {
    }

    public static GrupoParametrosDto getInstance() {
        synchronized (GrupoParametrosDto.class) {
            if (instance == null) {
                instance = new GrupoParametrosDto();
            }
        }
        return instance;
    }

}
