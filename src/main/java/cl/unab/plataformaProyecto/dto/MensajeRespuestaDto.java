package cl.unab.plataformaProyecto.dto;

import lombok.Data;

/**
 *
 * @author Christopher Lazo.
 */
@Data
public class MensajeRespuestaDto {
    
    private String mensaje;

    public MensajeRespuestaDto(String mensaje) {
        this.mensaje = mensaje;
    }
    
}
