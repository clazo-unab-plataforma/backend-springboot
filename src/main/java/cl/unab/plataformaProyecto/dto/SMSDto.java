package cl.unab.plataformaProyecto.dto;

import lombok.Data;

/**
 *
 * @author chris
 */
@Data
public class SMSDto {
    
    private String telefonoDestinatario;
    private String codigoValidacion;
    
}
