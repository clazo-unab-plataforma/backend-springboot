package cl.unab.plataformaProyecto.dto;

import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author Christopher Lazo.
 */
@Data
public class RolDto implements Serializable {

    private static final long serialVersionUID = -2378615016355332031L;

    private Long id;
    private String nombreRol;
    private String descripcion;

    public RolDto() {
    }

    public RolDto(Long id, String nombreRol) {
        this.id = id;
        this.nombreRol = nombreRol;
    }

}
