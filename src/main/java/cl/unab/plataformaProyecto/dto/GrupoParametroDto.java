package cl.unab.plataformaProyecto.dto;

import lombok.Data;

/**
 *
 * @author Christopher Lazo.
 */
@Data
public class GrupoParametroDto {
    
    private String idenfiticador;
    private String nombre;
    
}
