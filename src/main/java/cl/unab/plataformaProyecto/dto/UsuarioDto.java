package cl.unab.plataformaProyecto.dto;

import java.io.Serializable;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Christopher Lazo.
 */
@Data
public class UsuarioDto implements Serializable {

    private static final long serialVersionUID = -1232350001333774259L;

    private Long idUsuario;
    private Long rut;
    private String dv;
    private String username;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String telefono;
    private String email;
    private boolean activo;
    private List<RolDto> roles;

}
