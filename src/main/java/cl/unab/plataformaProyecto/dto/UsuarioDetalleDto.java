package cl.unab.plataformaProyecto.dto;

import java.util.List;
import lombok.Data;

/**
 *
 * @author Christopher Lazo.
 */
@Data
public class UsuarioDetalleDto {
    
    private Long idUsuario;
    private Long rut;
    private String dv;
    private String username;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String telefono;
    private String email;
    private boolean activo;
    private boolean tienePassword;
    private List<RolDto> roles;
    
}
