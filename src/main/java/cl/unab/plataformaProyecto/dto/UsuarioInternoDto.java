package cl.unab.plataformaProyecto.dto;

import java.io.Serializable;
import java.util.List;
import lombok.Data;

/**
 *
 * @author chris
 */
@Data
public class UsuarioInternoDto implements Serializable {

    private static final long serialVersionUID = 530306553889717039L;

    private Long idUsuario;
    private Long rut;
//    private String dv;
    private String username;
    private String password;
    private List<RolDto> roles;
    private String telefono;
    private String email;
    private String codValidacion;

    public UsuarioInternoDto(Long idUsuario, Long rut, String username, String password, String telefono, String codValidacion) {
        this.idUsuario = idUsuario;
        this.rut = rut;
//        this.dv = dv;
        this.username = username;
        this.password = password;
        this.telefono = telefono;
        this.codValidacion = codValidacion;
    }

}
