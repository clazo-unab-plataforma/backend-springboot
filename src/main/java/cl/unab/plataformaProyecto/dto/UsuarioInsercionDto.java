package cl.unab.plataformaProyecto.dto;

import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author Christopher Lazo.
 */
@Data
public class UsuarioInsercionDto extends UsuarioDto implements Serializable {

    private static final long serialVersionUID = 4824440127405484641L;
    private String password;
    
}
