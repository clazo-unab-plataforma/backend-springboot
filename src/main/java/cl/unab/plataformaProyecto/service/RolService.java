package cl.unab.plataformaProyecto.service;

import cl.unab.plataformaProyecto.dto.RolDto;
import cl.unab.plataformaProyecto.entity.RolEntity;


/**
 *
 * @author Christopher Lazo.
 */
public interface RolService {

    /**
     * Transforma un objeto de tipo Rol a RolDto.
     *
     * @param rolEntity objeto a transformar.
     * @return
     */
    public RolDto RolEntityADto(RolEntity rolEntity);

    /**
     * Transforma un objeto de tipo RolDto a Rol
     *
     * @param RolDto objeto a transformar.
     * @return
     */
    public RolEntity RolDtoAEntity(RolDto RolDto);

}
