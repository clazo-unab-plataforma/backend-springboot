package cl.unab.plataformaProyecto.service;

import cl.unab.plataformaProyecto.dto.SmsRequest;

/**
 *
 * @author chris
 */
public interface SmsService {

    /**
     * Envia un mensaje de texto.
     *
     * @param smsRequest
     */
    void sendSms(SmsRequest smsRequest);

    /**
     * Valida si un numero cumple con las condiciones para recibir SMS.
     *
     * @param number
     * @return
     */
    boolean isValidNumber(String number);

}
