package cl.unab.plataformaProyecto.service;

import cl.unab.plataformaProyecto.dto.MensajeRespuestaDto;
import cl.unab.plataformaProyecto.dto.UsuarioDetalleDto;
import cl.unab.plataformaProyecto.dto.UsuarioDto;
import cl.unab.plataformaProyecto.dto.UsuarioInsercionDto;
import cl.unab.plataformaProyecto.dto.UsuarioInternoDto;
import cl.unab.plataformaProyecto.entity.UsuarioEntity;

/**
 *
 * @author chris
 */
public interface UsuarioService {

    /**
     * retorna un usuario mediante su username obtenido desde el token de
     * acceso.
     *
     * @return
     */
    public UsuarioDetalleDto obtenerUsuarioDesdeToken();

    /**
     * Valida si un usuario tiene el rol Administrador.
     *
     * @param usuario usuario a validar.
     * @return
     */
    public boolean usuarioEsAdministrador(UsuarioDetalleDto usuario);

    /**
     * Inserta un usuario.
     *
     * @param usuario usuario a persistir
     * @return
     */
    public UsuarioDetalleDto insertarUsuario(UsuarioInsercionDto usuario);

    /**
     * edita a un usuario.
     *
     * @param usuario usuario a editar
     * @return
     */
    public UsuarioDetalleDto editarUsuario(UsuarioInsercionDto usuario);

    /**
     * Desactiva a un usuario ya existente para que no pueda autenticarse.
     *
     * @param idUsuario identificador unico de usuario a desactivar.
     */
    public void desactivarUsuario(Long idUsuario);

    /**
     * activa a un usuario ya existente para que pueda autenticarse.
     *
     * @param idUsuario identificador unico de usuario a activar.
     */
    public void activarUsuario(Long idUsuario);

    public UsuarioEntity usuarioDtoAEntity(UsuarioDto usuario);

    /**
     * retorna un usuario mediante su ID
     *
     * @param idUsuario identificador unico de usuario.
     * @return
     */
    public UsuarioDetalleDto obtenerUsuarioPorID(Long idUsuario);

    /**
     * Para la autenticacion mediante LDAP, retorna un usuario segun su
     * username.
     *
     * @param username username de usuario a retornar.
     * @return
     */
    public UsuarioInternoDto obtenerUsuarioInternoPorUsername(String username);

    /**
     * Realiza la primera autenticacion para login.
     *
     * @param username
     * @param password
     * @return
     */
    public MensajeRespuestaDto primeraAuthUsuario(String username, String password);

}
