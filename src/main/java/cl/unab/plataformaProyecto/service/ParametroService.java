package cl.unab.plataformaProyecto.service;

import cl.unab.plataformaProyecto.dto.ParametroDto;
import cl.unab.plataformaProyecto.entity.ParametroEntity;
import java.util.List;

/**
 *
 * @author Christopher Lazo.
 */
public interface ParametroService {

    /**
     * Retorna un listado de parametros por su grupo.
     *
     * @param grupo grupo de parametros.
     * @return
     */
    public List<ParametroDto> obtenerParametrosPorGrupo(String grupo);

    /**
     * Retorna un parametro por su grupo y codigo identificador.
     *
     * @param grupo grupo de parametro.
     * @param codigo codigo identificador de parametro dentro del grupo.
     * @return
     */
    public ParametroDto obtenerParametrosPorGrupoYCodigo(String grupo, Integer codigo);

    /**
     * obtiene todos los parametros existentes.
     *
     * @return
     */
    public List<ParametroDto> obtenerParametros();

    /**
     * obtiene todos los parametros existentes.
     *
     * @return
     */
    public List<ParametroDto> obtenerParametrosDesdeBD();
    
    public void cargarParametros();
    
    /**
     * obtiene todos los parametros existentes.
     *
     * @return
     */
    public void actualizarParametros();
    
    /**
     * Transforma un objeto Parametro a ParametroDto.
     * @param parametroEntity
     * @return 
     */
    public ParametroDto parametroEntityADto(ParametroEntity parametroEntity);
}
